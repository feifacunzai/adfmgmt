﻿using Autofac;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.ViewModels;
using System.Reflection;

namespace MgmtForm.BL.Modules
{
    public class ValidatorModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var mappings = Assembly.Load("MgmtForm.BL.Validators");                        

            builder.RegisterAssemblyTypes(mappings)
                   .Where(i => i.Name.EndsWith("Validator"))
                   .As(i => typeof(IValidator<>).MakeGenericType(i.BaseType.GetGenericArguments()));
        }
    }
}
