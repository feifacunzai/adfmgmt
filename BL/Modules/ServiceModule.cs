﻿using MgmtForm.BL.Interfaces;
using MgmtForm.BL.Services;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac.Extras.DynamicProxy2;
using MgmtForm.Utility.Extensions.Aop;

namespace MgmtForm.BL.Modules
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var service = Assembly.Load("MgmtForm.BL.Services");

            builder.RegisterAssemblyTypes(service)
                   .AsImplementedInterfaces()
                   .EnableInterfaceInterceptors();

        }
    }
}
