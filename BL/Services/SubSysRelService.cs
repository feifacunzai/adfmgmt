﻿using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.BL.Interfaces;
using MgmtForm.DA.Interfaces;
using System;
using System.Collections.Generic;
using MgmtForm.DA.Tables;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Services
{
    public class SubSysRelService : ISubSysRelService
    {
        public ISubSysRelRepository SubSysRelRepository { get; set; }


        public SubSysRelService(ISubSysRelRepository subSysRel)
        {
            this.SubSysRelRepository = subSysRel;
        }


        public void Create(SubSysRelEntity instance)
        {
            var model = Mapper.Map<SubSysRel>(instance);
            this.SubSysRelRepository.Create(model);
        }

        public IEnumerable<SubSysRelEntity> Read(Expression<Func<SubSysRelEntity, bool>> linq)
        {
            var models = this.SubSysRelRepository.Read(null);
            var entitys = Mapper.Map<IEnumerable<SubSysRelEntity>>(models);
            var result = (linq != null) ? entitys.Where(linq.Compile()) : entitys;
            return result;
        }

        public void Update(SubSysRelEntity instance)
        {
            var model = Mapper.Map<SubSysRel>(instance);
            this.SubSysRelRepository.Update(model, model.ID);
        }


        public void Delete(SubSysRelEntity instance)
        {
            var model = Mapper.Map<SubSysRel>(instance);
            model.IsValid = false;
            this.SubSysRelRepository.Delete(model, model.ID);
        }
    }
}
