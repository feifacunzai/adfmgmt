﻿using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.BL.Interfaces;
using MgmtForm.BL.Services.Misc;
using MgmtForm.DA.Interfaces;
using System;
using System.Collections.Generic;
using MgmtForm.DA.Tables;
using MgmtForm.Utility.Extensions;
using MgmtForm.ViewModels;
using SsoReference;

namespace MgmtForm.BL.Services
{
    public class SubSysService : ISubSysService
    {
        public ISubSysRepository SubSysRepository { get; set; }


        public ISubSysDetService SubSysDetService { get; set; }


        public ISubSysRelService SubSysRelService { get; set; }


        public SubSysService(ISubSysRepository subSys, ISubSysDetService subSysDet, ISubSysRelService subSysRel)
        {
            this.SubSysRepository = subSys;
            this.SubSysDetService = subSysDet;
            this.SubSysRelService = subSysRel;
        }


        public void Create(SubSysEntity instance)
        {

            throw new NotImplementedException();
        }


        public IEnumerable<SubSysEntity> Read(Expression<Func<SubSysEntity, bool>> linq)
        {
            var models = this.SubSysRepository.Read(null);
            var entitys = Mapper.Map<IEnumerable<SubSysEntity>>(models);
            var result = (linq != null) ? entitys.Where(linq.Compile()) : entitys;
            return result;
        }


        public void Update(SubSysEntity request)
        {
            if (!this.SubSysRepository.IsExists(s => s.CompUuid == request.CompUuid && s.ServiceUuid == request.ServiceUuid))
            {
                throw new Exception("站台不存在");
            }

            var instance =
                this.SubSysRepository.Read(s => s.CompUuid == request.CompUuid && s.ServiceUuid == request.ServiceUuid)
                    .FirstOrDefault();

            try
            {
                var check = false;
                switch (instance.Status)
                {
                    case "22":
                        if (request.Status == "40")
                        {
                            var instanceDet = this.SubSysDetService.Read(s => s.SubSysid == instance.ID && bool.Parse(s.IsEnabled)).FirstOrDefault();
                            if (instanceDet == null ||
                                string.IsNullOrEmpty(instanceDet.DataBaseid) ||
                                string.IsNullOrEmpty(instanceDet.ExtIP))
                            {
                                throw new Exception("該站台尚未設置DB");
                            }
                            check = true;
                            instance.Status = "23";
                            this.SubSysRepository.Update(instance, instance.ID);
                            //同步公司、部門、員工
                            var subSysEntity = Mapper.Map<SubSysEntity>(instance);
                            var sync = new SyncCenter();
                            if (!sync.SyncComp(subSysEntity))
                            {
                                throw new Exception("同步公司失敗");
                            }
                            if (!sync.SyncCompanyConfig(subSysEntity))
                            {
                                throw new Exception("同步公司設定檔失敗");
                            }
                            if (!sync.SyncApiConfig(subSysEntity, instanceDet))
                            {
                                throw new Exception("同步API設定檔失敗");
                            }
                            if (!sync.SyncServiceInitial(subSysEntity))
                            {
                                throw new Exception("表單初始化失敗");
                            }
                            if (!sync.SyncDept(subSysEntity))
                            {
                                throw new Exception("同步部門失敗");
                            }
                            if (!sync.SyncUser(subSysEntity))
                            {
                                throw new Exception("同步員工失敗");
                            }
                        }
                        break;
                    case "45":
                        if (request.Status == "50") check = true;
                        break;
                    case "56":
                        if (request.Status == "60") check = true;
                        break;
                }

                if (check)
                {
                    instance.Status = request.Status;
                    this.SubSysRepository.Update(instance, instance.ID);

                    if (instance.Status== "60")
                    {
                        var subSysDet = this.SubSysDetService.Read(s => s.SubSysid == instance.ID).FirstOrDefault();
                        var subSysRel = this.SubSysRelService.Read(s => s.SubSysDetid == subSysDet.ID).FirstOrDefault();

                        this.Delete(request);
                        this.SubSysDetService.Delete(subSysDet);
                        this.SubSysRelService.Delete(subSysRel);
                    }
                }
                else
                {
                    throw new Exception("不符合操作項目");
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public void Delete(SubSysEntity instance)
        {
            var model = Mapper.Map<SubSys>(instance);
            model.IsValid = false;
            this.SubSysRepository.Delete(model, model.ID);
        }


        public SsoAddInstanceResultEntity add_an_instance(SsoAddInstanceRequestEntity request)
        {
            Guid compUuid;
            if (!Guid.TryParse(request.APP_COMPANY_UUID, out compUuid))
            {
                throw new Exception("公司代碼錯誤");
            }

            Guid serviceUuid;
            if (!Guid.TryParse(request.APP_SERVICE_UUID, out serviceUuid))
            {
                throw new Exception("服務代碼錯誤");
            }

            if (this.SubSysRepository.IsExists(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid))
            {
                throw new Exception("站台代碼重複");
            }

            var result = new SsoAddInstanceResultEntity();

            if (request.EXECUTION_FLAG == "1")
            {
                try
                {
                    SsoCacheCenter ssoCacheCenter = new SsoCacheCenter();
                    SsoTransfer ssoTransfer = new SsoTransfer();
                    var subSysUuid = Guid.NewGuid();
                    this.SubSysRepository.Create(new SubSys()
                    {
                        ID = subSysUuid,
                        CompUuid = compUuid,
                        ServiceUuid = serviceUuid,
                        CompId = ssoTransfer.GetAnInstance(
                            ssoCacheCenter.GetInternalPrivilegedToken().PrivilegedToken,
                            compUuid.ToString()
                        ).APP_COMPANY_BASIC_PROFILE.APP_COMPANY_ID,
                        Status = "15"
                    });
                    var subSysDetUuid = Guid.NewGuid();
                    this.SubSysDetService.Create(new SubSysDetEntity()
                    {
                        ID = subSysDetUuid,
                        CompUuid = compUuid,
                        ServiceUuid = serviceUuid,
                        SubSysid = subSysUuid
                    });
                    var subSysRelUuid = Guid.NewGuid();
                    this.SubSysRelService.Create(new SubSysRelEntity()
                    {
                        ID = subSysRelUuid,
                        CompUuid = compUuid,
                        ServiceUuid = serviceUuid,
                        SubSysDetid = subSysDetUuid
                    });

                    var instance =
                        this.SubSysRepository.Read(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid).FirstOrDefault();
                    result = Mapper.Map<SsoAddInstanceResultEntity>(instance);

                    MailReport mailReport = new MailReport();
                    mailReport.SendEmailByNewErpSite(instance.CompId, instance.Status);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                
            }

            return result;

        }


        public void delete_an_instance(SsoDeleteInstanceRequestEntity request)
        {
            Guid compUuid;
            if (!Guid.TryParse(request.APP_COMPANY_UUID, out compUuid))
            {
                throw new Exception("公司代碼錯誤");
            }

            Guid serviceUuid;
            if (!Guid.TryParse(request.APP_SERVICE_UUID, out serviceUuid))
            {
                throw new Exception("服務代碼錯誤");
            }

            if (!this.SubSysRepository.IsExists(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid))
            {
                throw new Exception("站台不存在");
            }

            if (request.EXECUTION_FLAG == "1")
            {
                try
                {
                    var instance = this.SubSysRepository.Read(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid).FirstOrDefault();

                    switch (instance.Status)
                    {
                        case "50":
                            instance.Status = "56";
                            this.SubSysRepository.Update(instance, instance.ID);
                            MailReport mailReport = new MailReport();
                            mailReport.SendEmailByNewErpSite(instance.CompId, instance.Status);
                            break;
                        default:
                            throw new Exception("不符合操作項目");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public void disable_an_instance(SsoDisableInstanceRequestEntity request)
        {
            Guid compUuid;
            if (!Guid.TryParse(request.APP_COMPANY_UUID, out compUuid))
            {
                throw new Exception("公司代碼錯誤");
            }

            Guid serviceUuid;
            if (!Guid.TryParse(request.APP_SERVICE_UUID, out serviceUuid))
            {
                throw new Exception("服務代碼錯誤");
            }

            if (!this.SubSysRepository.IsExists(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid))
            {
                throw new Exception("站台不存在");
            }

            if (request.EXECUTION_FLAG == "1")
            {
                try
                {
                    var instance = this.SubSysRepository.Read(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid).FirstOrDefault();

                    switch (instance.Status)
                    {
                        case "40":
                            instance.Status = "45";
                            this.SubSysRepository.Update(instance, instance.ID);
                            break;
                        default:
                            throw new Exception("不符合操作項目");
                    }
                    
                    instance.IsEnabled = false;
                    this.SubSysRepository.Update(instance, instance.ID);
                    var subSysDetEntity = this.SubSysDetService.Read(s => s.SubSysid == instance.ID).FirstOrDefault();
                    subSysDetEntity.IsEnabled = false.ToString();
                    this.SubSysDetService.Update(subSysDetEntity);

                    MailReport mailReport = new MailReport();
                    mailReport.SendEmailByNewErpSite(instance.CompId, instance.Status);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public void ensable_an_instance(SsoEnableInstanceRequestEntity request)
        {
            Guid compUuid;
            if (!Guid.TryParse(request.APP_COMPANY_UUID, out compUuid))
            {
                throw new Exception("公司代碼錯誤");
            }

            Guid serviceUuid;
            if (!Guid.TryParse(request.APP_SERVICE_UUID, out serviceUuid))
            {
                throw new Exception("服務代碼錯誤");
            }

            if (!this.SubSysRepository.IsExists(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid))
            {
                throw new Exception("站台不存在");
            }

            if (request.EXECUTION_FLAG == "1")
            {
                try
                {
                    var instance = this.SubSysRepository.Read(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid).FirstOrDefault();

                    switch (instance.Status)
                    {
                        case "50":
                            instance.Status = "54";
                            this.SubSysRepository.Update(instance, instance.ID);
                            break;
                        default:
                            throw new Exception("不符合操作項目");
                    }
                    
                    instance.IsEnabled = true;
                    this.SubSysRepository.Update(instance, instance.ID);
                    var subSysDetEntity = this.SubSysDetService.Read(s => s.SubSysid == instance.ID).FirstOrDefault();
                    subSysDetEntity.IsEnabled = true.ToString();
                    this.SubSysDetService.Update(subSysDetEntity);

                    MailReport mailReport = new MailReport();
                    mailReport.SendEmailByNewErpSite(instance.CompId, instance.Status);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }


        public SsoModifyBasicProfileResultEntity modify_basic_profile(SsoModifyBasicProfileRequestEntity request)
        {
            Guid compUuid;
            if (!Guid.TryParse(request.APP_COMPANY_UUID, out compUuid))
            {
                throw new Exception("公司代碼錯誤");
            }

            Guid serviceUuid;
            if (!Guid.TryParse(request.APP_SERVICE_UUID, out serviceUuid))
            {
                throw new Exception("服務代碼錯誤");
            }

            if (!this.SubSysRepository.IsExists(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid))
            {
                throw new Exception("站台不存在");
            }

            if (ReferenceEquals(request.APP_ADF_BASIC_PROFILE, null))
            {
                throw new Exception("未輸入站台基本屬性");
            }

            var result = new SsoModifyBasicProfileResultEntity();

            if (request.EXECUTION_FLAG == "1")
            {
                try
                {
                    var instance = this.SubSysRepository.Read(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid).FirstOrDefault();

                    switch (instance.Status)
                    {
                        //第一次修改
                        case "15":
                            instance.Status = "16";
                            this.SubSysRepository.Update(instance, instance.ID);

                            //同步位置服務中心
                            var subSysEntity = Mapper.Map<SubSysEntity>(instance);
                            var sync = new SyncCenter();
                            if (!sync.SyncLocationService(subSysEntity))
                            {
                                throw new Exception("同步位置服務中心失敗");
                            }

                            instance.Status = "20";
                            break;
                        //非第一次修改
                        case "40":
                            instance.Status = "41";
                            this.SubSysRepository.Update(instance, instance.ID);
                            instance.Status = "40";
                            break;
                        default:
                            throw new Exception("不符合操作項目");
                    }

                    instance.Name = request.APP_ADF_BASIC_PROFILE.APP_ADF_CHT_NAME;
                    instance.DESC = request.APP_ADF_BASIC_PROFILE.APP_ADF_CHT_DESC;
                    this.SubSysRepository.Update(instance, instance.ID);
                    result = Mapper.Map<SsoModifyBasicProfileResultEntity>(instance);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return result;
        }


        public SsoModifyResourceProfileResultEntity modify_resource_profile(SsoModifyResourceProfileRequestEntity request)
        {
            Guid compUuid;
            if (!Guid.TryParse(request.APP_COMPANY_UUID, out compUuid))
            {
                throw new Exception("公司代碼錯誤");
            }

            Guid serviceUuid;
            if (!Guid.TryParse(request.APP_SERVICE_UUID, out serviceUuid))
            {
                throw new Exception("服務代碼錯誤");
            }

            if (!this.SubSysRepository.IsExists(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid))
            {
                throw new Exception("站台不存在");
            }

            if (ReferenceEquals(request.APP_ADF_RESOURCE_PROFILE, null))
            {
                throw new Exception("未輸入站台資源屬性");
            }

            int Storage_max;
            if (!int.TryParse(request.APP_ADF_RESOURCE_PROFILE.ADF_STORAGE_MAX, out Storage_max))
            {
                throw new Exception("服務最大空間錯誤");
            }
            int User_max;
            if (!int.TryParse(request.APP_ADF_RESOURCE_PROFILE.ADF_USER_MAX, out User_max))
            {
                throw new Exception("服務最大人數錯誤");
            }

            var result = new SsoModifyResourceProfileResultEntity();

            if (request.EXECUTION_FLAG == "1")
            {
                try
                {
                    var instance = this.SubSysRepository.Read(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid).FirstOrDefault();

                    switch (instance.Status)
                    {
                        //第一次修改
                        case "20":
                            instance.Status = "21";
                            this.SubSysRepository.Update(instance, instance.ID);
                            instance.Status = "22";
                            break;
                        //非第一次修改
                        case "40":
                            instance.Status = "41";
                            this.SubSysRepository.Update(instance, instance.ID);
                            instance.Status = "40";
                            break;
                        default:
                            throw new Exception("不符合操作項目");
                    }
                    
                    instance.Storage_max = request.APP_ADF_RESOURCE_PROFILE.ADF_STORAGE_MAX;
                    instance.User_max = request.APP_ADF_RESOURCE_PROFILE.ADF_USER_MAX;
                    instance.IsEnabled = true;
                    this.SubSysRepository.Update(instance, instance.ID);
                    var subSysDetEntity = this.SubSysDetService.Read(s => s.SubSysid == instance.ID).FirstOrDefault();
                    subSysDetEntity.IsEnabled = true.ToString();
                    this.SubSysDetService.Update(subSysDetEntity);
                    result = Mapper.Map<SsoModifyResourceProfileResultEntity>(instance);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

            return result;
        }


        public SsoGetInstanceResultEntity get_an_instance(SsoGetInstanceRequestEntity request)
        {
            Guid compUuid;
            if (!Guid.TryParse(request.APP_COMPANY_UUID, out compUuid))
            {
                throw new Exception("公司代碼錯誤");
            }

            Guid serviceUuid;
            if (!Guid.TryParse(request.APP_SERVICE_UUID, out serviceUuid))
            {
                throw new Exception("服務代碼錯誤");
            }

            if (!this.SubSysRepository.IsExists(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid, true))
            {
                throw new Exception("站台不存在");
            }

            var result = new SsoGetInstanceResultEntity();

            var instance = this.SubSysRepository.Read(s => s.CompUuid == compUuid && s.ServiceUuid == serviceUuid, true).FirstOrDefault();

            result = Mapper.Map<SsoGetInstanceResultEntity>(instance);

            return result;

        }

    }
}
