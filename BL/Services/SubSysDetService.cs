﻿using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.BL.Interfaces;
using MgmtForm.DA.Interfaces;
using System;
using System.Collections.Generic;
using MgmtForm.DA.Tables;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Services
{
    public class SubSysDetService : ISubSysDetService
    {
        public ISubSysDetRepository SubSysDetRepository { get; set; }


        public SubSysDetService(ISubSysDetRepository subSysDet)
        {
            this.SubSysDetRepository = subSysDet;
        }


        public void Create(SubSysDetEntity instance)
        {
            var model = Mapper.Map<SubSysDet>(instance);
            this.SubSysDetRepository.Create(model);
        }


        public IEnumerable<SubSysDetEntity> Read(Expression<Func<SubSysDetEntity, bool>> linq)
        {
            var models = this.SubSysDetRepository.Read(null);
            var entitys = Mapper.Map<IEnumerable<SubSysDetEntity>>(models);
            var result = (linq != null) ? entitys.Where(linq.Compile()) : entitys;
            return result;
        }


        public void Update(SubSysDetEntity instance)
        {
            var model = Mapper.Map<SubSysDet>(instance);
            this.SubSysDetRepository.Update(model, model.ID);
        }


        public void Delete(SubSysDetEntity instance)
        {
            var model = Mapper.Map<SubSysDet>(instance);
            model.IsValid = false;
            this.SubSysDetRepository.Delete(model, model.ID);
        }
    }
}
