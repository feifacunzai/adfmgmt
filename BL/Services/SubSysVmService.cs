﻿using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.BL.Interfaces;
using MgmtForm.DA.Interfaces;
using System;
using System.Collections.Generic;
using MgmtForm.DA.Tables;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Services
{
    public class SubSysVmService : ISubSysVmService
    {
        public ISubSysVmRepository SubSysVmRepository { get; set; }


        public SubSysVmService(ISubSysVmRepository subSysVm)
        {
            this.SubSysVmRepository = subSysVm;
        }


        public void Create(SubSysVmEntity instance)
        {
            var model = Mapper.Map<SubSysVm>(instance);
            this.SubSysVmRepository.Create(model);
        }


        public IEnumerable<SubSysVmEntity> Read(Expression<Func<SubSysVmEntity, bool>> linq)
        {
            var models = this.SubSysVmRepository.Read(null);
            var entitys = Mapper.Map<IEnumerable<SubSysVmEntity>>(models);
            var result = (linq != null) ? entitys.Where(linq.Compile()) : entitys;
            return result;
        }


        public void Update(SubSysVmEntity instance)
        {
            throw new NotImplementedException();
        }


        public void Delete(SubSysVmEntity instance)
        {
            var model = Mapper.Map<SubSysVm>(instance);
            model.IsValid = false;
            this.SubSysVmRepository.Delete(model, model.ID);
        }
    }
}
