﻿using MgmtForm.BL.Interfaces;
using MgmtForm.DA.Interfaces;
using MgmtForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.BL.Services
{
    public class SampleService : ISampleService
    {
        public ISampleRepository SampleRepository { get; set; }        

        public SampleService(ISampleRepository sampleRepository)
        {
            this.SampleRepository = sampleRepository;
        }

        public IEnumerable<SampleModel> GetSamples()
        {
            return this.SampleRepository.GetSamples();
        }
    }
}
