﻿using System.Linq.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Interfaces
{
    public interface IBaseService<TEntity>
        where TEntity : class 
    {
        void Create(TEntity instance);

        IEnumerable<TEntity> Read(Expression<Func<TEntity, bool>> linq);

        void Update(TEntity instance);

        void Delete(TEntity instance);
    }
}
