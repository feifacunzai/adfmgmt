﻿using System;
using System.Collections.Generic;
using MgmtForm.BL.Services.Misc;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Interfaces
{
    public interface ISubSysService : IBaseService<SubSysEntity>
    {
        SsoAddInstanceResultEntity add_an_instance(SsoAddInstanceRequestEntity request);

        void delete_an_instance(SsoDeleteInstanceRequestEntity request);

        void disable_an_instance(SsoDisableInstanceRequestEntity request);

        void ensable_an_instance(SsoEnableInstanceRequestEntity request);

        SsoModifyBasicProfileResultEntity modify_basic_profile(SsoModifyBasicProfileRequestEntity request);

        SsoModifyResourceProfileResultEntity modify_resource_profile(SsoModifyResourceProfileRequestEntity request);

        SsoGetInstanceResultEntity get_an_instance(SsoGetInstanceRequestEntity request);

    }
}
