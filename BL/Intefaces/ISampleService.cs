﻿using MgmtForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.BL.Interfaces
{
    public interface ISampleService
    {
        IEnumerable<SampleModel> GetSamples();
    }
}
