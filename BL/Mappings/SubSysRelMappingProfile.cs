﻿using System;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.DA.Tables;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Mappings
{
    public class SubSysRelMappingProfile : Profile
    {

        public override string ProfileName
        {
            get
            {
                return "SubSysRelMappingProfile";
            }
        }


        protected override void Configure()
        {
            Mapper.CreateMap<SubSysRel, SubSysRelEntity>();

            Mapper.CreateMap<SubSysRelEntity, SubSysRel>()
                .ForMember(s => s.CreateDate, t => t.Ignore())
                .ForMember(s => s.UpdateDate, t => t.Ignore());

        }
    }
}
