﻿using System;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.DA.Tables;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Mappings
{
    public class BaseMappingProfile : Profile
    {

        public override string ProfileName
        {
            get
            {
                return "BaseMappingProfile";
            }
        }


        protected override void Configure()
        {
            Mapper.CreateMap<DateTime, string>().ConvertUsing(new DateTimeTypeConverter());

            //Mapper.CreateMap<string, string>().ConvertUsing(new StringTypeConverter());

        }


        public class DateTimeTypeConverter : ITypeConverter<DateTime, string>
        {
            public string Convert(ResolutionContext context)
            {
                return System.Convert.ToDateTime(context.SourceValue).ToString("yyyyMMddHHmmsszz00");
            }
        }


        public static class FunctionCompositionExtensions
        {
            public static Expression<Func<X, Y>> Compose<X, Y, Z>(Expression<Func<Z, Y>> outer, Expression<Func<X, Z>> inner)
            {
                return Expression.Lambda<Func<X, Y>>(
                    ParameterReplacer.Replace(outer.Body, outer.Parameters[0], inner.Body),
                    inner.Parameters[0]);
            }
        }


        //public class StringTypeConverter : ITypeConverter<string, string>
        //{
        //    public string Convert(ResolutionContext context)
        //    {
        //        if (context.SourceValue != null)
        //        {
        //            return context.SourceValue.ToString();
        //        }
        //        else
        //        {
        //            return "";
        //        }
        //    }
        //}

    }

}
