﻿using System;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.DA.Tables;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Mappings
{
    public class SubSysDetMappingProfile : Profile
    {

        public override string ProfileName
        {
            get
            {
                return "SubSysDetMappingProfile";
            }
        }


        protected override void Configure()
        {
            Mapper.CreateMap<SubSysDet, SubSysDetEntity>();

            Mapper.CreateMap<SubSysDetEntity, SubSysDet>()
                .ForMember(s => s.CreateDate, t => t.Ignore())
                .ForMember(s => s.UpdateDate, t => t.Ignore());

        }
    }
}
