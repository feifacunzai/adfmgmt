﻿using System;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.DA.Tables;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Mappings
{
    public class SubSysMappingProfile : Profile
    {

        public override string ProfileName
        {
            get
            {
                return "SubSysMappingProfile";
            }
        }


        protected override void Configure()
        {
            Mapper.CreateMap<SubSys, SubSysEntity>();
                
            Mapper.CreateMap<SubSysEntity, SubSys>()
                .ForMember(s => s.CreateDate, t => t.Ignore())
                .ForMember(s => s.UpdateDate, t => t.Ignore());

            Mapper.CreateMap<SubSys, SsoAddInstanceResultEntity>()
                .ForMember(s => s.APP_ADF_UUID, t => t.MapFrom(s => s.ServiceUuid))
                .ForMember(s => s.APP_ADF_LAST_UPDATE_TIME, t => t.MapFrom(s => s.UpdateDate))
                .ForMember(s => s.APP_ADF_LAST_UPDATE_TAG, t => t.MapFrom(s => s.Status));

            Mapper.CreateMap<SubSys, SsoModifyBasicProfileResultEntity>()
                .ForMember(s => s.APP_ADF_LAST_UPDATE_TIME, t => t.MapFrom(s => s.UpdateDate))
                .ForMember(s => s.APP_ADF_LAST_UPDATE_TAG, t => t.MapFrom(s => s.Status));

            Mapper.CreateMap<SubSys, SsoModifyResourceProfileResultEntity>()
                .ForMember(s => s.APP_ADF_LAST_UPDATE_TIME, t => t.MapFrom(s => s.UpdateDate))
                .ForMember(s => s.APP_ADF_LAST_UPDATE_TAG, t => t.MapFrom(s => s.Status));

            Mapper.CreateMap<SubSys, SsoGetInstanceResultEntity>()
                .ForMember(s => s.APP_ADF_LAST_UPDATE_TAG, t => t.MapFrom(s => s.Status))
                .ForMember(s => s.APP_ADF_LAST_UPDATE_TIME, t => t.MapFrom(s => s.UpdateDate))
                .ForMember(s => s.APP_ADF_STATUS, t => t.MapFrom(s => s.Status))
                .ForMember(s => s.APP_ADF_BASIC_PROFILE, t => t.MapFrom(s => s))
                .ForMember(s => s.APP_ADF_RESOURCE_PROFILE, t => t.MapFrom(s => s));

            Mapper.CreateMap<SubSys, SsoAdfBasicProfileEntity>()
                .ForMember(s => s.APP_ADF_CHT_DESC, t => t.MapFrom(s => s.DESC))
                .ForMember(s => s.APP_ADF_CHT_NAME, t => t.MapFrom(s => s.Name))
                .ForMember(s => s.APP_ADF_SERVICE_PATH, t => t.UseValue("adf.aoacloud.com.tw"));

            Mapper.CreateMap<SubSys, SsoAdfResourceProfileEntity>()
                .ForMember(s => s.ADF_STORAGE_MAX, t => t.MapFrom(s => s.Storage_max))
                .ForMember(s => s.ADF_USER_MAX, t => t.MapFrom(s => s.User_max));


        }
    }
}
