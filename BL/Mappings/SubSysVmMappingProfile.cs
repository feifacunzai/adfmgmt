﻿using System;
using System.Linq.Expressions;
using AutoMapper;
using MgmtForm.DA.Tables;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Mappings
{
    public class SubSysVmMappingProfile : Profile
    {

        public override string ProfileName
        {
            get
            {
                return "SubSysVmMappingProfile";
            }
        }


        protected override void Configure()
        {
            Mapper.CreateMap<SubSysVm, SubSysVmEntity>();

            Mapper.CreateMap<SubSysVmEntity, SubSysVm>()
                .ForMember(s => s.CreateDate, t => t.Ignore())
                .ForMember(s => s.UpdateDate, t => t.Ignore());

        }
    }
}
