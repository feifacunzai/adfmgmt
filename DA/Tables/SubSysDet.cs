﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.Utility.Hooks.Audit;

namespace MgmtForm.DA.Tables
{
    [Table("SubSysDet")]
    public partial class SubSysDet : EntityBase
    {
        public Guid ID { get; set; }

        public Guid SubSysid { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

        [StringLength(20)]
        public string ExtIP { get; set; }

        [StringLength(10)]
        public string DataBaseid { get; set; }

        public bool IsEnabled { get; set; }

    }

    [MetadataType(typeof(SubSysDetMetadata))]
    public partial class SubSysDet
    {
    }

    public class SubSysDetMetadata
    {
        public Guid ID { get; set; }

        public Guid SubSysid { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

        public string ExtIP { get; set; }

        public string DataBaseid { get; set; }

        public bool IsEnabled { get; set; }

    }
}
