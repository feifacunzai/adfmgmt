﻿using MgmtForm.Utility.Hooks.Audit;
using EFHooks;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.DA.Tables
{
    public class ShopContext : HookedDbContext, IAuditableContext
    {
        /// <summary>
        /// For update-database by package management console
        /// </summary>
        public ShopContext()
        {
        }

        /// <summary>
        /// For runtime
        /// </summary>
        /// <param name="hooks"></param>
        public ShopContext(IEnumerable<IPreActionHook> hooks)
        {
            foreach (var hook in hooks)
            {
                this.RegisterHook(hook);
            }
        }

        public IDbSet<AuditLog> AuditLogs { get; set; }

    }
}
