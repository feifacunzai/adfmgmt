﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.Utility.Hooks.Audit;

namespace MgmtForm.DA.Tables
{
    [Table("SubSysVM")]
    public partial class SubSysVm : EntityBase
    {
        public Guid ID { get; set; }

        [StringLength(2)]
        public string Type { get; set; }

        [StringLength(20)]
        public string VMHostName { get; set; }

        [StringLength(20)]
        public string VMIP { get; set; }

        [StringLength(50)]
        public string Account { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        [StringLength(400)]
        public string DESC { get; set; }

        public bool IsEnable { get; set; }

    }

    [MetadataType(typeof(SubSysVmMetadata))]
    public partial class SubSysVm
    {
    }

    public class SubSysVmMetadata
    {
        public Guid ID { get; set; }

        public string Type { get; set; }

        public string VMHostName { get; set; }

        public string VMIP { get; set; }

        public string Account { get; set; }

        public string Password { get; set; }

        public string DESC { get; set; }

        public bool IsEnable { get; set; }

    }
}
