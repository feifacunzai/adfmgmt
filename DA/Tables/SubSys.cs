﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.Utility.Hooks.Audit;

namespace MgmtForm.DA.Tables
{
    [Table("SubSys")]
    public partial class SubSys : EntityBase
    {
        public Guid ID { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

        [StringLength(36)]
        public string CompId { get; set; }

        [StringLength(50)]
        public string ExtFqdn { get; set; }

        [StringLength(3)]
        public string Status { get; set; }

        [StringLength(400)]
        public string Name { get; set; }

        [StringLength(400)]
        public string DESC { get; set; }

        [StringLength(10)]
        public string Storage_max { get; set; }

        [StringLength(10)]
        public string User_max { get; set; }

        public bool IsEnabled { get; set; }

    }

    [MetadataType(typeof(SubSysMetadata))]
    public partial class SubSys
    {
    }

    public class SubSysMetadata
    {
        public Guid ID { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

        public string CompId { get; set; }

        public string ExtFqdn { get; set; }

        public string Status { get; set; }

        public string Name { get; set; }

        public string DESC { get; set; }

        public string Storage_max { get; set; }

        public string User_max { get; set; }

        public bool IsEnabled { get; set; }

    }
}
