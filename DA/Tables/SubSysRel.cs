﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.Utility.Hooks.Audit;

namespace MgmtForm.DA.Tables
{
    [Table("SubSysRel")]
    public partial class SubSysRel : EntityBase
    {
        public Guid ID { get; set; }

        public Guid SubSysDetid { get; set; }

        public Guid? SubSysVMid { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

    }

    [MetadataType(typeof(SubSysRelMetadata))]
    public partial class SubSysRel
    {
    }

    public class SubSysRelMetadata
    {
        public Guid ID { get; set; }

        public Guid SubSysDetid { get; set; }

        public Guid SubSysVMid { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

    }
}
