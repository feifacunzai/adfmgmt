﻿using MgmtForm.Utility.Hooks.Audit;
using EFHooks;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.DA.Tables
{
    public class MgmtContext : HookedDbContext, IAuditableContext
    {
        /// <summary>
        /// For update-database by package management console
        /// </summary>
        public MgmtContext()
            : base("MgmtForm.DA.Tables.MgmtContext")
        {
        }

        /// <summary>
        /// For runtime
        /// </summary>
        /// <param name="hooks"></param>
        public MgmtContext(IEnumerable<IPreActionHook> hooks)
            : base("MgmtForm.DA.Tables.MgmtContext")
        {
            foreach (var hook in hooks)
            {
                this.RegisterHook(hook);
            }
        }


        /// <summary>
        /// 這是code first必須的
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<MgmtContext>(null);
        }


        public IDbSet<AuditLog> AuditLogs { get; set; }

        public IDbSet<SubSys> SubSys { get; set; }

        public IDbSet<SubSysDet> SubSysDet { get; set; }

        public IDbSet<SubSysRel> SubSysRel { get; set; } 

        public IDbSet<SubSysVm> SubSysVm { get; set; } 

    }
}
