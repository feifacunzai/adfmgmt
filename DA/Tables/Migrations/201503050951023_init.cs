namespace MgmtForm.DA.Tables.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IdentifyKey = c.Guid(nullable: false),
                        IdentifyName = c.String(),
                        OriginValue = c.String(),
                        NewValue = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        CreateUser = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubSys",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CompUuid = c.Guid(nullable: false),
                        ServiceUuid = c.Guid(nullable: false),
                        CompId = c.String(maxLength: 36),
                        ExtFqdn = c.String(maxLength: 50),
                        Status = c.String(maxLength: 3),
                        Name = c.String(maxLength: 400),
                        DESC = c.String(maxLength: 400),
                        Storage_max = c.String(maxLength: 10),
                        User_max = c.String(maxLength: 10),
                        IsEnabled = c.Boolean(nullable: false),
                        CreateUser = c.String(maxLength: 36),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateUser = c.String(maxLength: 36),
                        UpdateDate = c.DateTime(nullable: false),
                        IsValid = c.Boolean(nullable: false),
                        IdentifyKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubSysDet",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SubSysid = c.Guid(nullable: false),
                        CompUuid = c.Guid(nullable: false),
                        ServiceUuid = c.Guid(nullable: false),
                        ExtIP = c.String(maxLength: 20),
                        DataBaseid = c.String(maxLength: 10),
                        IsEnabled = c.Boolean(nullable: false),
                        CreateUser = c.String(maxLength: 36),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateUser = c.String(maxLength: 36),
                        UpdateDate = c.DateTime(nullable: false),
                        IsValid = c.Boolean(nullable: false),
                        IdentifyKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubSysRel",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SubSysDetid = c.Guid(nullable: false),
                        SubSysVMid = c.Guid(),
                        CompUuid = c.Guid(nullable: false),
                        ServiceUuid = c.Guid(nullable: false),
                        CreateUser = c.String(maxLength: 36),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateUser = c.String(maxLength: 36),
                        UpdateDate = c.DateTime(nullable: false),
                        IsValid = c.Boolean(nullable: false),
                        IdentifyKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SubSysVM",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.String(maxLength: 2),
                        VMHostName = c.String(maxLength: 20),
                        VMIP = c.String(maxLength: 20),
                        Account = c.String(maxLength: 50),
                        Password = c.String(maxLength: 50),
                        DESC = c.String(maxLength: 400),
                        IsEnable = c.Boolean(nullable: false),
                        CreateUser = c.String(maxLength: 36),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateUser = c.String(maxLength: 36),
                        UpdateDate = c.DateTime(nullable: false),
                        IsValid = c.Boolean(nullable: false),
                        IdentifyKey = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SubSysVM");
            DropTable("dbo.SubSysRel");
            DropTable("dbo.SubSysDet");
            DropTable("dbo.SubSys");
            DropTable("dbo.AuditLogs");
        }
    }
}
