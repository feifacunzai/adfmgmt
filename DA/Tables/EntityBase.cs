﻿using MgmtForm.Utility.Hooks.Audit;
using MgmtForm.Utility.Hooks.UpdateSystemInfo;
using MgmtForm.Utility.Hooks.ValidFlag;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.DA.Tables
{
    public class EntityBase : ISystemInfo, IIsValid, IIdentifiable
    {
        public EntityBase()
        {
            this.CreateDate = DateTime.Now;
            this.CreateUser = "Unlogin";
            this.UpdateDate = DateTime.Now;
            this.UpdateUser = "Unlogin";
            this.IsValid = true;
            this.IdentifyKey = Guid.NewGuid();
        }

        [StringLength(36)]
        public string CreateUser { get; set; }

        public DateTime CreateDate { get; set; }

        [StringLength(36)]
        public string UpdateUser { get; set; }

        public DateTime UpdateDate { get; set; }

        public bool IsValid { get; set; }

        public Guid IdentifyKey { get; set; }
    }
}
