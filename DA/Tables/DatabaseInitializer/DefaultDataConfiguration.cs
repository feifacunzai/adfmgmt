﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.DA.Tables.DatabaseInitializer
{
    public class DefaultDataConfiguration : DbMigrationsConfiguration<ShopContext>
    {
        public DefaultDataConfiguration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ShopContext context)
        {

        }
    }
}
