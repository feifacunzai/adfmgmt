﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.Models;

namespace MgmtForm.DA.Interfaces
{
    public interface ISampleRepository
    {
        IEnumerable<SampleModel> GetSamples();
    }
}
