﻿using MgmtForm.DA.Tables;

namespace MgmtForm.DA.Interfaces
{
    public interface ISubSysRepository : IBaseRepository<SubSys>
    {

    }
}
