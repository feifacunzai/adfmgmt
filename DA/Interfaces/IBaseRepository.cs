﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.DA.Interfaces
{
    public interface IBaseRepository<TEntity> : IDisposable
        where TEntity :class
    {
        void Create(TEntity instance);

        IQueryable<TEntity> Read(Expression<Func<TEntity, bool>> linq, bool includeDelete=false);

        void Update(TEntity instance, params object[] keyValue);

        void Delete(TEntity instance, params object[] keyValue);

        bool IsExists(Expression<Func<TEntity, bool>> linq, bool includeDelete=false);

    }
}
