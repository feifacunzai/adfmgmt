﻿using MgmtForm.DA.Tables;

namespace MgmtForm.DA.Interfaces
{
    public interface ISubSysVmRepository : IBaseRepository<SubSysVm>
    {

    }
}
