﻿using MgmtForm.DA.Tables;

namespace MgmtForm.DA.Interfaces
{
    public interface ISubSysRelRepository : IBaseRepository<SubSysRel>
    {

    }
}
