﻿using MgmtForm.DA.Tables;

namespace MgmtForm.DA.Interfaces
{
    public interface ISubSysDetRepository : IBaseRepository<SubSysDet>
    {

    }
}
