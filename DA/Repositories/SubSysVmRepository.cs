﻿using System.Data.Entity;
using MgmtForm.DA.Interfaces;
using MgmtForm.DA.Tables;
using MgmtForm.Utility.Hooks.ValidFlag;

namespace MgmtForm.DA.Repositories
{
    public class SubSysVmRepository : BaseRepository<SubSysVm>, ISubSysVmRepository
    {
        public SubSysVmRepository(MgmtContext dbContext)
            : base(dbContext)
        {
            this.MgmtContext = dbContext;
            this.QuerableValid = dbContext.SubSysVm.Valids();
        }

    }
}
