﻿using System.Data.Entity;
using MgmtForm.DA.Interfaces;
using MgmtForm.DA.Tables;
using MgmtForm.Utility.Hooks.ValidFlag;

namespace MgmtForm.DA.Repositories
{
    public class SubSysDetRepository : BaseRepository<SubSysDet>, ISubSysDetRepository
    {
        public SubSysDetRepository(MgmtContext dbContext)
            : base(dbContext)
        {
            this.MgmtContext = dbContext;
            this.QuerableValid = dbContext.SubSysDet.Valids();
        }

    }
}
