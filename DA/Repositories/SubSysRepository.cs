﻿using System.Data.Entity;
using MgmtForm.DA.Interfaces;
using MgmtForm.DA.Tables;
using MgmtForm.Utility.Hooks.ValidFlag;

namespace MgmtForm.DA.Repositories
{
    public class SubSysRepository : BaseRepository<SubSys>, ISubSysRepository
    {
        public SubSysRepository(MgmtContext dbContext)
            : base(dbContext)
        {
            this.MgmtContext = dbContext;
            this.QuerableValid = dbContext.SubSys.Valids();
        }

    }
}
