﻿using MgmtForm.DA.Interfaces;
using MgmtForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.DA.Repositories
{
    public class SampleRepository : ISampleRepository
    {
        public IEnumerable<SampleModel> GetSamples()
        {
            for (int i = 0; i < 10; i++)
            {
                yield return new SampleModel()
                {
                    Id = i,
                    Data = string.Format("Data - {0}", i),
                    CreatedAt = DateTime.Now
                };
            }
        }
    }
}
