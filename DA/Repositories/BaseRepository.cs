﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using EFHooks;
using MgmtForm.DA.Interfaces;
using MgmtForm.DA.Tables;
using MgmtForm.Utility.Hooks.ValidFlag;

namespace MgmtForm.DA.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : class
    {
        public MgmtContext MgmtContext { get; set; }

        public IQueryable<TEntity> QuerableValid { get; set; } 


        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="mgmtContext"></param>
        public BaseRepository(MgmtContext mgmtContext)
        {
            this.MgmtContext = mgmtContext;
        }


        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="instance"></param>
        public void Create(TEntity instance)
        {
            if (instance == null)
            {
                throw new ArgumentNullException("未實體化物件" + typeof(TEntity));
            }
            else
            {
                this.MgmtContext.Set<TEntity>().Add(instance);
                this.SaveChanges();
            }
        }


        /// <summary>
        /// 查詢
        /// </summary>
        /// <param name="linq"></param>
        public IQueryable<TEntity> Read(Expression<Func<TEntity, bool>> linq, bool includeValid=false)
        {
            IQueryable<TEntity> source = null;
            source = includeValid 
                ? this.MgmtContext.Set<TEntity>() 
                : this.QuerableValid;
            return (linq == null) 
                ? source 
                : source.Where(linq);
        }


        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="instance"></param>
        public void Update(TEntity instance, params object[] keyValue)
        {
            if (instance == null)
            {
                throw new ArgumentNullException("未實體化物件" + typeof(TEntity));
            }
            else
            {
                var entry = this.MgmtContext.Entry<TEntity>(instance);
                var set = this.MgmtContext.Set<TEntity>();

                TEntity attachedEntity = set.Find(keyValue);
                if (attachedEntity != null)
                {
                    var attachedEntry = this.MgmtContext.Entry(attachedEntity);
                    attachedEntry.CurrentValues.SetValues(instance);
                }
                else
                {
                    entry.State = EntityState.Modified;
                }
                this.SaveChanges();
            }
        }


        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="instance"></param>
        public void Delete(TEntity instance, params object[] keyValue)
        {
            if (instance == null)
            {
                throw new ArgumentNullException("未實體化物件" + typeof(TEntity));
            }
            else
            {
                var entry = this.MgmtContext.Entry<TEntity>(instance);
                var set = this.MgmtContext.Set<TEntity>();

                TEntity attachedEntity = set.Find(keyValue);
                if (attachedEntity != null)
                {
                    var attachedEntry = this.MgmtContext.Entry(attachedEntity);
                    attachedEntry.CurrentValues.SetValues(instance);
                }
                else
                {
                    entry.State = EntityState.Deleted;
                }
                this.SaveChanges();
            }
        }


        /// <summary>
        /// 檢查是否存在
        /// </summary>
        /// <param name="linq"></param>
        /// <param name="IncludeValid">是否包含已刪除資料</param>
        /// <returns></returns>
        public bool IsExists(Expression<Func<TEntity, bool>> linq, bool includeDelete=false)
        {
            return (linq == null) 
                ? this.Read(null, includeDelete).Any() 
                : this.Read(null, includeDelete).Any(linq);
        }


        /// <summary>
        /// 儲存
        /// </summary>
        public void SaveChanges()
        {
            this.MgmtContext.SaveChanges();
        }


        /// <summary>
        /// 清除連結
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.MgmtContext != null)
                {
                    this.MgmtContext.Dispose();
                    this.MgmtContext = null;
                }
            }
        }

    }
}
