﻿using EFHooks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MgmtForm.Utility.Hooks.UpdateSystemInfo
{
    public class UpdateSystemInfoPreInsertHook : PreInsertHook<ISystemInfo>
    {
        public HttpContextBase HttpContext { get; set; }

        public UpdateSystemInfoPreInsertHook(HttpContextBase httpContext)
        {
            this.HttpContext = httpContext;
        }

        public override void Hook(ISystemInfo entity, HookEntityMetadata metadata)
        {
            var userName = "Unlogin";
            if (this.HttpContext != null && 
                this.HttpContext.User.Identity.IsAuthenticated)
            {
                userName = this.HttpContext.User.Identity.Name;
            }

            entity.CreateUser = userName;
            entity.CreateDate = DateTime.Now;
            entity.UpdateUser = userName;
            entity.UpdateDate = DateTime.Now;
        }

        public override bool RequiresValidation
        {
            get { return false; }
        }
    }
}
