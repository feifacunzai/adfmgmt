﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.Utility.Hooks.UpdateSystemInfo
{
    public interface ISystemInfo
    {
        string CreateUser { get; set; }

        DateTime CreateDate { get; set; }

        string UpdateUser { get; set; }

        DateTime UpdateDate { get; set; }
    }
}
