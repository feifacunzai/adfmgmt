﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.Utility.Hooks.ValidFlag
{
    public interface IIsValid
    {
        bool IsValid { get; set; }
    }
}
