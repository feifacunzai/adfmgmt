﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.Utility.Hooks.Audit
{
    public interface IIdentifiable
    {
        Guid IdentifyKey { get; set; }
    }
}
