﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace MgmtForm.Utility.Extensions
{
    public class MailReport
    {
        public string SendEmailByNewErpSite(string compId, string status)
        {
            var receiveList = WebConfigurationManager.AppSettings["MailTo"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            string result = string.Empty;
            try
            {
                var title = compId;
                var bodyBuilder = new StringBuilder();
                bodyBuilder.AppendLine("Hi , 您好：");
                bodyBuilder.AppendLine("<br/><br/>");

                if (status == "15")
                {
                    //有異動資源屬性    
                    title += " 新ADF站台建置需求單";

                    bodyBuilder.AppendLine("有家新公司 " + compId + " 需要<span style='color:red;'>建立</span>新ADF站台！");
                }
                else if (status == "45")
                {
                    //有停用ADF
                    title += " ADF站台停用單";

                    bodyBuilder.AppendLine("有家公司 " + compId + " 需要<span style='color:red;'>停用</span>ADF站台！");
                }
                else if (status == "56")
                {
                    //有刪除ADF
                    title += " ADF站台刪除單";

                    bodyBuilder.AppendLine("有家公司 " + compId + " 需要<span style='color:red;'>刪除</span>ADF站台！");
                }
                else if (status == "54")
                {
                    //有啟用ADF
                    title += " ADF站台啟用單";

                    bodyBuilder.AppendLine("有家公司 " + compId + " 需要<span style='color:red;'>啟用</span>ADF站台！");
                }

                bodyBuilder.AppendLine("<br/><br/>");
                bodyBuilder.AppendLine("請至 <a href='ADFmgmt.aoacloud.com.tw'>adfmgmt.aoacloud.com.tw</a> 進行操作。");
                bodyBuilder.AppendLine("<br/><br/><br/>");
                bodyBuilder.AppendLine("系統發送，請勿回覆信件。");

                MailMessage mail = new MailMessage();

                SmtpClient smtpServer = new SmtpClient("smtp.aoacloud.com.tw");
                smtpServer.Credentials = new System.Net.NetworkCredential("erp", "b1fa7ee889ae185186e651d412e619ac");
                //smtpServer.Port = 587; // Gmail works on this port
                smtpServer.Port = 25;

                mail.From = new MailAddress("mgmt-ADF@aoacloud.com.tw");

                if (receiveList.Any())
                {
                    foreach (var receive in receiveList)
                    {
                        mail.To.Add(receive);
                    }
                }

                mail.Subject = title;
                mail.Body = bodyBuilder.ToString();
                mail.IsBodyHtml = true;

                smtpServer.Send(mail);

                result = "0";
            }
            catch (Exception)
            {
                result = "1";
            }

            return result;
        }
    }
}
