﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using MgmtForm.ViewModels;
using Newtonsoft.Json;
using SsoReference;

namespace MgmtForm.Utility.Extensions
{
    public class PowerShellCommand
    {
        /// <summary>
        /// 執行 PowerShell 指令
        /// </summary>
        /// <param name="scriptText">PowerShell 指令</param>
        private string InvokePowerShellCommand(string scriptText)
        {
            // create Powershell runspace

            Runspace runspace = RunspaceFactory.CreateRunspace();

            // open it

            runspace.Open();

            // create a pipeline and feed it the script text

            Pipeline pipeline = runspace.CreatePipeline();
            pipeline.Commands.AddScript(scriptText);
   
            // add an extra command to transform the script
            // output objects into nicely formatted strings

            // remove this line to get the actual objects
            // that the script returns. For example, the script

            // "Get-Process" returns a collection
            // of System.Diagnostics.Process instances.

            pipeline.Commands.Add("Out-String");
  
            // execute the script
            Collection<PSObject> results = new Collection<PSObject>();
            try
            {
                results = pipeline.Invoke();
            }
            catch (Exception ex)
            {
                runspace.Close();
                throw new Exception(ex.Message);
            }
        
            // close the runspace

            runspace.Close();

            // convert the script result into a single string

            StringBuilder stringBuilder = new StringBuilder();
            foreach (PSObject obj in results)
            {
                stringBuilder.AppendLine(obj.ToString());
            }

            return stringBuilder.ToString();
        }


        public string GetDbgenCmd(string dataBaseId, string apIpAddress, string dbIpAddress, string compUuId, string dbAccount, string dbPassword)
        {
            var cmd = String.Format(
                        WebConfigurationManager.AppSettings["PowerShellPath"] +
                        WebConfigurationManager.AppSettings["PowerShellFile"] +
                        @" -dbname {0} -apip1 {1} -apip2 {2} -dbip {3} -compuuid {4} –dbac {5} –dbpw {6} -serviceType {7}"
                        , dataBaseId
                        , apIpAddress
                        , apIpAddress
                        , dbIpAddress
                        , compUuId
                        , dbAccount
                        , dbPassword
                        , WebConfigurationManager.AppSettings["ServiceType"]);
            return cmd;
        }

        public string Exec(string cmd)
        {
            return InvokePowerShellCommand(cmd);
        }




    }
}
