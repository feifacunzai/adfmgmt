﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using MgmtForm.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SsoReference;

namespace MgmtForm.Utility.Extensions
{
    public class SyncCenter
    {
        private SsoCacheCenter SsoCacheCenter = new SsoCacheCenter();

        private Guid MgmtPrivilegedSsoToken { get; set; }

        public string SERVICE_TYPE = WebConfigurationManager.AppSettings["ServiceType2"];
        public string VDC_ID = WebConfigurationManager.AppSettings["VDC_ID"];
        public string INT_IP_ADDR = WebConfigurationManager.AppSettings["INT_IP_ADDR"];
        public string INT_VC_ADDR = WebConfigurationManager.AppSettings["INT_VC_ADDR"];
        public string EXT_IP_ADDR = WebConfigurationManager.AppSettings["EXT_IP_ADDR"];
        public string INT_FQDN = WebConfigurationManager.AppSettings["INT_FQDN"];
        public string EXT_FQDN = WebConfigurationManager.AppSettings["EXT_FQDN"];


        public SyncCenter()
        {
            this.MgmtPrivilegedSsoToken = Guid.Parse(this.SsoCacheCenter.GetInternalPrivilegedToken().PrivilegedToken);
        }


        private string PostAsJsonAsync(string uri, string method, Object requestJSON)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(uri);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = null;

            try
            {
                response = client.PostAsJsonAsync(method, requestJSON).Result;
            }
            catch (Exception e)
            {
                throw new Exception(uri + method + ": " + e.InnerException + "(連不到目標網址)");
            }

            var body = response.Content.ReadAsStringAsync().Result;
            dynamic dynamicBody = JObject.Parse(body);
            string checkError = (dynamicBody.ERROR_MESSAGE != null)
                ? dynamicBody.ERROR_MESSAGE.ToString()
                : (dynamicBody.ErrorMessage != null)
                    ? dynamicBody.ErrorMessage.ToString()
                    : null;
            if (!string.IsNullOrEmpty(checkError))
            {
                this.ElmahLoger(checkError);
            }

            if (response.StatusCode.ToString() != "OK")
            {
                body = "{}";
            }
            return body;
        }


        public bool SyncComp(SubSysEntity subSysEntity)
        {
            bool result = false;

            #region 取得單一公司基本站台
            object companyGetCompanyRequest = new
            {
                PRIVILEGED_APP_SSO_TOKEN = this.MgmtPrivilegedSsoToken,
                APP_COMPANY_UUID = subSysEntity.CompUuid
            };
            var companyGetCompanyResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["PublicApiUri"], "Company/GetCompany", companyGetCompanyRequest);
            var companyGetCompanyJson = JsonConvert.DeserializeObject<PublicResultEntity>(companyGetCompanyResult);
            #endregion

            if (companyGetCompanyJson.ErrorCode != "0")
            {
                #region 新增單一公司基本站台

                object companyCreateRequest = new
                {
                    PRIVILEGED_APP_SSO_TOKEN = this.MgmtPrivilegedSsoToken,
                    APP_COMPANY_UUID = subSysEntity.CompUuid,
                    EXECUTION_FLAG = "1"
                };
                var companyCreateResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["PublicApiUri"],
                    "Company/Create", companyCreateRequest);
                var companyCreateJson = JsonConvert.DeserializeObject<PublicResultEntity>(companyCreateResult);

                #endregion


                #region 啟用單一公司基本站台

                object companyEnableRequest = new
                {
                    PRIVILEGED_APP_SSO_TOKEN = this.MgmtPrivilegedSsoToken,
                    APP_COMPANY_UUID = subSysEntity.CompUuid,
                    EXECUTION_FLAG = "1"
                };
                var companyEnableResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["PublicApiUri"],
                    "Company/Enable", companyEnableRequest);
                var companyEnableJson = JsonConvert.DeserializeObject<PublicResultEntity>(companyEnableResult);

                #endregion


                #region 異動單一公司基本站台之基本屬性

                object companyUpdateRequest = new
                {
                    PRIVILEGED_APP_SSO_TOKEN = this.MgmtPrivilegedSsoToken,
                    APP_COMPANY_UUID = subSysEntity.CompUuid,
                    APP_COMPANY_BASIC_PROFILE = new
                    {
                        APP_COMPANY_ID = subSysEntity.CompId,
                        APP_COMPANY_NAME = subSysEntity.Name,
                        APP_COMPANY_DESC = subSysEntity.DESC,
                        APP_COMPANY_CODE = subSysEntity.CompId,
                        APP_COMPANY_VAT_NO = ""
                    },
                    EXECUTION_FLAG = "1"
                };
                var companyUpdateResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["PublicApiUri"],
                    "Company/Update", companyUpdateRequest);
                var companyUpdateJson = JsonConvert.DeserializeObject<PublicResultEntity>(companyUpdateResult);

                #endregion

                if (companyCreateJson.ErrorCode == "0" &&
                    companyEnableJson.ErrorCode == "0" &&
                    companyUpdateJson.ErrorCode == "0")
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }



            return result;
        }


        public bool SyncUser(SubSysEntity subSysEntity)
        {
            #region 完整同步員工
            object syncUserRequest = new
            {
                PRIVILEGED_APP_SSO_TOKEN = this.MgmtPrivilegedSsoToken,
                APP_COMPANY_UUID = subSysEntity.CompUuid,
                APP_SERVICE_UUID = subSysEntity.ServiceUuid,
                EXECUTION_FLAG = "1"
            };
            var syncUserResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["PublicApiUri"], "Sync/User", syncUserRequest);
            var syncUserJson = JsonConvert.DeserializeObject<PublicResultEntity>(syncUserResult);
            #endregion

            return (syncUserJson.ErrorCode == "0");
        }


        public bool SyncDept(SubSysEntity subSysEntity)
        {
            #region 完整同步部門
            object syncDeptRequest = new
            {
                PRIVILEGED_APP_SSO_TOKEN = this.MgmtPrivilegedSsoToken,
                APP_COMPANY_UUID = subSysEntity.CompUuid,
                APP_SERVICE_UUID = subSysEntity.ServiceUuid,
                EXECUTION_FLAG = "1"
            };
            var syncDeptResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["PublicApiUri"], "Sync/Dept", syncDeptRequest);
            var syncDeptJson = JsonConvert.DeserializeObject<PublicResultEntity>(syncDeptResult);
            #endregion

            return (syncDeptJson.ErrorCode == "0");
        }


        public bool SyncLocationService(SubSysEntity subSysEntity)
        {
            var internalIps = this.INT_IP_ADDR.Split(new string[] {","}, StringSplitOptions.RemoveEmptyEntries);
            Boolean result = true;

            foreach (var internalIp in internalIps)
            {
                #region 同步位置服務中心(WS-Z06-01a)
                object syncLocationServiceRequestTypeA = new
                {
                    PRIVILEGED_APP_SSO_TOKEN = this.MgmtPrivilegedSsoToken,
                    APP_COMPANY_UUID = subSysEntity.CompUuid,
                    APP_SERVICE_UUID = subSysEntity.ServiceUuid,
                    LOCATION_INFO = new
                    {
                        VDC_ID = this.VDC_ID,
                        INT_IP_ADDR = internalIp,
                        INT_VC_ADDR = this.INT_VC_ADDR,
                        EXT_IP_ADDR = this.EXT_IP_ADDR,
                        INT_FQDN = this.INT_FQDN,
                        EXT_FQDN = this.EXT_FQDN
                    }
                };
                var syncLocationServiceResultTypeA = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["SsoApiUri"], "location_service/add_an_location/", syncLocationServiceRequestTypeA);
                var syncLocationServiceResultJsonTypeA = JsonConvert.DeserializeObject<SsoResultEntity>(syncLocationServiceResultTypeA);
                if (syncLocationServiceResultJsonTypeA.ERROR_CODE != "0")
                {
                    result = false;
                }
                #endregion

                #region 同步位置服務中心(WS-Z06-01c)
                object syncLocationServiceRequestTypeC = new
                {
                    PRIVILEGED_APP_SSO_TOKEN = this.MgmtPrivilegedSsoToken,
                    PUBLIC_APP_ID_TO_REGISTER = WebConfigurationManager.AppSettings["AppPrivateId"], //appPrivateId
                    LOCATION_INFO = new
                    {
                        VDC_ID = this.VDC_ID,
                        INT_IP_ADDR = internalIp,
                        INT_VC_ADDR = this.INT_VC_ADDR,
                        EXT_IP_ADDR = this.EXT_IP_ADDR,
                        INT_FQDN = this.INT_FQDN,
                        EXT_FQDN = this.EXT_FQDN
                    }
                };
                var syncLocationServiceResultTypeC = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["SsoApiUri"], "location_service/add_an_location/", syncLocationServiceRequestTypeC);
                var syncLocationServiceResultJsonTypeC = JsonConvert.DeserializeObject<SsoResultEntity>(syncLocationServiceResultTypeC);
                if (syncLocationServiceResultJsonTypeC.ERROR_CODE != "0")
                {
                    result = false;
                }
                #endregion
            }

            return result;
        }

        public bool SyncCompanyConfig(SubSysEntity subSysEntity)
        {
            #region 同步公司設定檔
            object syncCompanyConfigRequest = new
            {
                SsoToken = this.MgmtPrivilegedSsoToken,
                CompUuid = subSysEntity.CompUuid,
                ServiceUuid = subSysEntity.ServiceUuid,
                ServiceType = this.SERVICE_TYPE
            };
            var syncCompanyConfigResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["PublicApiUri"], "CompConfig/Create", syncCompanyConfigRequest);
            var syncCompanyResultJson = JsonConvert.DeserializeObject<PublicResultEntity>(syncCompanyConfigResult);
            #endregion

            return (syncCompanyResultJson.ErrorCode == "0");
        }


        public bool SyncApiConfig(SubSysEntity subSysEntity, SubSysDetEntity subSysDetEntity)
        {
            #region 同步API設定檔
            var apiConfig = new PublicAddApiConfig()
            {
                SsoToken = this.MgmtPrivilegedSsoToken.ToString(),
                CompUuid = subSysEntity.CompUuid.ToString(),
                ServiceUuid = subSysEntity.ServiceUuid.ToString(),
                Fields = new PublicApiConfigProfileEntity()
                {
                    CompUUID = subSysEntity.CompUuid.ToString(),
                    ServiceUUID = subSysEntity.ServiceUuid.ToString(),
                    Uri = "*",
                    Method = "*",
                    ContentType = "application/json",
                    DbConnection = "Server=" + subSysDetEntity.ExtIP + ";Database=" + subSysDetEntity.DataBaseid + ";",
                    ServiceType = this.SERVICE_TYPE
                }
            };
            var syncApiConfigResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["PublicApiUri"], "ApiConfig/Create", apiConfig);
            var syncApiResultJson = JsonConvert.DeserializeObject<PublicResultEntity>(syncApiConfigResult);
            #endregion

            return (syncApiResultJson.ErrorCode == "0");
        }


        public bool SyncServiceInitial(SubSysEntity subSysEntity)
        {
            #region 初始化表單服務
            object syncServiceInitialRequest = new
            {
                SsoToken = this.MgmtPrivilegedSsoToken,
                CompUuid = subSysEntity.CompUuid,
                ServiceUuid = subSysEntity.ServiceUuid
            };
            var syncServiceInitialResult = this.PostAsJsonAsync(WebConfigurationManager.AppSettings["DocFormApiUri"], "Menu/Create", syncServiceInitialRequest);
            var syncServiceInitialJson = JsonConvert.DeserializeObject<AdfResultEntity>(syncServiceInitialResult);
            #endregion

            return syncServiceInitialJson.ErrorCode == "0";
        }


        public void ElmahLoger(string logStr)
        {
            Elmah.ErrorSignal.FromCurrentContext().Raise(
                new Exception("SyncError:" + logStr)
                );
        }


    }
}
