﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class DatabaseMatchPage
    {
        public SubSysEntity MatchData { get; set; }

        public List<DatabaseEntity> DatabaseList { get; set; }

        public List<SubSysVmEntity> VmList { get; set; }

    }
}
