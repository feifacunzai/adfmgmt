﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class CompanyEntity
    {
        public Guid Id { get; set; }

        [Display(Name = "公司外部代碼")]
        public string CompanyId { get; set; }

        [Display(Name = "資料庫代碼")]
        public string DatabaseId { get; set; }

        [Display(Name = "資料庫IP")]
        public string DatabaseIp { get; set; }

        [Display(Name = "公司UUID")]
        public string CompanyUuid { get; set; }

        [Display(Name = "服務UUID")]
        public string ServiceUuid { get; set; }

        [Display(Name = "公司名稱")]
        public string CompanyName { get; set; }

        [Display(Name = "使用空間")]
        public string Storage { get; set; }

        [Display(Name = "使用人數")]
        public string UserLimit { get; set; }

        [Display(Name = "建立時間")]
        public string CreateDate { get; set; }

        [Display(Name = "狀態")]
        public string Status { get; set; }

    }
}
