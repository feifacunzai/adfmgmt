﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class DatabaseEntity
    {
        public string Id { get; set; }

        [Display(Name = "資料庫代碼")]
        public string DatabaseId { get; set; }

        [Display(Name = "資料庫IP")]
        public string DatabaseIp { get; set; }

        [Display(Name = "DB總使用公司數")]
        public string ServiceTotal { get; set; }

        [Display(Name = "DB總使用人數")]
        public string DbTotalUser { get; set; }

        [Display(Name = "主機ID")]
        public string VmId { get; set; }

        [Display(Name = "主機類別")]
        public string VmType { get; set; }

        [Display(Name = "主機名稱")]
        public string VmHostName { get; set; }

        [Display(Name = "主機IP")]
        public string VmIp { get; set; }

        [Display(Name = "主機備註說明")]
        public string VmDesc { get; set; }

        [Display(Name = "Vm總使用人數")]
        public string VmTotalUser { get; set; }

    }
}
