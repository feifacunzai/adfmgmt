﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class SubSysVmEntity
    {
        public Guid ID { get; set; }

        public string Type { get; set; }

        public string VMHostName { get; set; }

        public string VMIP { get; set; }

        public string Account { get; set; }

        public string Password { get; set; }

        public string DESC { get; set; }

        public string IsEnable { get; set; }

        public string CreateUser { get; set; }

        public string CreateDate { get; set; }

        public string UpdateUser { get; set; }

        public string UpdateDate { get; set; }
    }
}
