﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class SubSysEntity
    {
        public Guid ID { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

        public string CompId { get; set; }

        public string ExtFqdn { get; set; }

        public string Status { get; set; }

        public string Name { get; set; }

        public string DESC { get; set; }

        public string Storage_max { get; set; }

        public string User_max { get; set; }

        public string IsEnabled { get; set; }

        public string CreateUser { get; set; }

        public string CreateDate { get; set; }

        public string UpdateUser { get; set; }

        public string UpdateDate { get; set; }
    }
}
