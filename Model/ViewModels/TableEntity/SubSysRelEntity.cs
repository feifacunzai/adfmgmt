﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class SubSysRelEntity
    {
        public Guid ID { get; set; }

        public Guid SubSysDetid { get; set; }

        public Guid SubSysVMid { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

        public string CreateUser { get; set; }

        public string CreateDate { get; set; }

        public string UpdateUser { get; set; }

        public string UpdateDate { get; set; }
    }
}
