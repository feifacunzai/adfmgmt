﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class PublicAddApiConfig
    {
        public string SsoToken { get; set; }

        public string CompUuid { get; set; }

        public string ServiceUuid { get; set; }

        public PublicApiConfigProfileEntity Fields { get; set; }

    }
}
