﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class SsoModifyBasicProfileRequestEntity
    {
        public string PRIVILEGED_APP_SSO_TOKEN { get; set; }

        public string APP_COMPANY_UUID { get; set; }

        public string APP_SERVICE_UUID { get; set; }

        public SsoAdfBasicProfileEntity APP_ADF_BASIC_PROFILE { get; set; }

        public string EXECUTION_FLAG { get; set; }
    }
}
