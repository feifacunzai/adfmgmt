﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class SsoResultEntity
    {
        public string ERROR_CODE { get; set; }

        public string ERROR_MESSAGE { get; set; }
    }
}
