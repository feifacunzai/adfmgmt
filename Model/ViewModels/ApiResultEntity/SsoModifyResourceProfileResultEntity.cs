﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.BL.Services.Misc;

namespace MgmtForm.ViewModels
{
    public class SsoModifyResourceProfileResultEntity
    {
        public string APP_ADF_LAST_UPDATE_TIME { get; set; }

        public string APP_ADF_LAST_UPDATE_TAG { get; set; }

    }
}
