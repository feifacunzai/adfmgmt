﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class PublicResultEntity
    {
        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}
