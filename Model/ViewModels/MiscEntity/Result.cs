﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MgmtForm.ViewModels;

namespace MgmtForm.BL.Services.Misc
{
    public class Result : IResult
    {
        public string ERROR_CODE { get; set; }

        public string ERROR_MESSAGE { get; set; }

    }
}
