﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class SsoAdfResourceProfileEntity
    {
        public string ADF_STORAGE_MAX { get; set; }

        public string ADF_USER_MAX { get; set; }

    }
}
