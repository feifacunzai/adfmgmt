﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class SsoAdfBasicProfileEntity
    {
        public string APP_ADF_CHT_NAME { get; set; }

        public string APP_ADF_CHT_DESC { get; set; }

        public string APP_ADF_SERVICE_PATH { get; set; }

    }
}
