﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.ViewModels
{
    public class PublicApiConfigProfileEntity
    {
        public string CompUUID { get; set; }

        public string ServiceUUID { get; set; }

        public string Uri { get; set; }

        public string Method { get; set; }

        public string ContentType { get; set; }

        public string DbConnection { get; set; }

        public string ServiceType { get; set; }

    }
}
