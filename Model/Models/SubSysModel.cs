﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MgmtForm.Models
{
    public class SubSys
    {
        public Guid ID { get; set; }

        public Guid CompUuid { get; set; }

        public Guid ServiceUuid { get; set; }

        public string CompId { get; set; }

        public string ExtFqdn { get; set; }

        public string Status { get; set; }

        public string Name { get; set; }

        public string DESC { get; set; }

        public string Storage_max { get; set; }

        public string User_max { get; set; }

        public bool IsEnabled { get; set; }
    }
}
