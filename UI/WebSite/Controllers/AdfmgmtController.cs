﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MgmtForm.BL.Interfaces;
using MgmtForm.UI.ActionFilters;
using MgmtForm.ViewModels;
using Newtonsoft.Json;

namespace MgmtForm.UI.Controllers
{
    public class AdfmgmtController : Controller
    {
        public ISubSysService ISubSysService { get; set; }


        public AdfmgmtController(ISubSysService iSubSysService)
        {
            this.ISubSysService = iSubSysService;
        }


        //[HttpPost]
        [AuthorizeByTokenAttribute]
        public ActionResult add_an_instance(SsoAddInstanceRequestEntity request)
        {
            var result = this.ISubSysService.add_an_instance(request);
            return Json(result, JsonRequestBehavior.DenyGet);
        }


        [AuthorizeByTokenAttribute]
        public ActionResult delete_an_instance(SsoDeleteInstanceRequestEntity request)
        {
            this.ISubSysService.delete_an_instance(request);
            return Json(null, JsonRequestBehavior.DenyGet);
        }


        [AuthorizeByTokenAttribute]
        public ActionResult disable_an_instance(SsoDisableInstanceRequestEntity request)
        {
            this.ISubSysService.disable_an_instance(request);
            return Json(null, JsonRequestBehavior.DenyGet);
        }


        [AuthorizeByTokenAttribute]
        public ActionResult enable_an_instance(SsoEnableInstanceRequestEntity request)
        {
            this.ISubSysService.ensable_an_instance(request);
            return Json(null, JsonRequestBehavior.DenyGet);
        }


        [AuthorizeByTokenAttribute]
        public ActionResult modify_basic_profile(SsoModifyBasicProfileRequestEntity request)
        {
            var result = this.ISubSysService.modify_basic_profile(request);
            return Json(result, JsonRequestBehavior.DenyGet);
        }


        [AuthorizeByTokenAttribute]
        public ActionResult modify_resource_profile(SsoModifyResourceProfileRequestEntity request)
        {
            var result = this.ISubSysService.modify_resource_profile(request);
            return Json(result, JsonRequestBehavior.DenyGet);
        }


        [AuthorizeByTokenAttribute]
        public ActionResult get_an_instance(SsoGetInstanceRequestEntity request)
        {
            var result = this.ISubSysService.get_an_instance(request);
            return Json(result, JsonRequestBehavior.DenyGet);
        }


    }
}
