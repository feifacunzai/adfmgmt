﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Elmah.Assertions;
using MgmtForm.BL.Interfaces;
using MgmtForm.Utility.Extensions;
using MgmtForm.ViewModels;
using Newtonsoft.Json;

namespace MgmtForm.UI.Controllers
{
    public class DatabaseController : Controller
    {

        ISubSysService SubSysService { get; set; }


        ISubSysDetService SubSysDetService { get; set; }


        ISubSysRelService SubSysRelService { get; set; }


        ISubSysVmService SubSysVmService { get; set; }


        public DatabaseController(ISubSysService subSys, ISubSysDetService subSysDet, ISubSysRelService subSysRel, ISubSysVmService subSysVm)
        {
            this.SubSysService = subSys;
            this.SubSysDetService = subSysDet;
            this.SubSysRelService = subSysRel;
            this.SubSysVmService = subSysVm;
        }

        //
        // GET: /Company/
        public ActionResult Index()
        {
            var data = this.GetDatabaseList();
            return View(data);
        }


        public ActionResult Match(string id)
        {
            var matchData = this.SubSysService.Read(s => s.ID == Guid.Parse(id)).FirstOrDefault();
            if (matchData == null)
            {
                TempData["AlertMessage"] = "資料不存在";
                return RedirectToAction("Index", "Company");
            }

            var data = new DatabaseMatchPage();
            data.MatchData = matchData;
            data.DatabaseList = this.GetDatabaseList();
            data.VmList = this.SubSysVmService.Read(s => s.Type == "DB").ToList();
            return View(data);
        }


        public ActionResult CopySubsysDet(FormCollection formdata)
        {
            var subsysId = formdata["subsysId"];
            var subsysData = this.SubSysService.Read(s => s.ID == Guid.Parse(subsysId)).FirstOrDefault();

            var becopySubsysDetId = formdata["Id"];
            var becopySubsysDetData = this.SubSysDetService.Read(s => s.ID == Guid.Parse(becopySubsysDetId)).FirstOrDefault();

            var updateSubsysDetData =
                this.SubSysDetService.Read(
                    s => s.CompUuid == subsysData.CompUuid && s.ServiceUuid == subsysData.ServiceUuid).FirstOrDefault();
            updateSubsysDetData.DataBaseid = becopySubsysDetData.DataBaseid;
            updateSubsysDetData.ExtIP = becopySubsysDetData.ExtIP;
            updateSubsysDetData.IsEnabled = true.ToString();
            this.SubSysDetService.Update(updateSubsysDetData);

            var becopySubsysRelData =
                this.SubSysRelService.Read(s => s.SubSysDetid == Guid.Parse(becopySubsysDetId)).FirstOrDefault();

            var updateSubsysRelData =
                this.SubSysRelService.Read(s => s.SubSysDetid == updateSubsysDetData.ID).FirstOrDefault();
            updateSubsysRelData.SubSysVMid = becopySubsysRelData.SubSysVMid;
            this.SubSysRelService.Update(updateSubsysRelData);

            return RedirectToAction("Index");
        }


        public ActionResult CreateSubsysDet(FormCollection formdata)
        {
            if (string.IsNullOrEmpty(formdata["SubsysId"]))
            {
                throw new Exception("缺少SubsysId參數");
            }
            if (string.IsNullOrEmpty(formdata["VmId"]))
            {
                throw new Exception("缺少VmId參數");
            }
            var subsysId = Guid.Parse(formdata["SubsysId"]);
            var subsysVmId = Guid.Parse(formdata["VmId"]);

            var subsysData = this.SubSysService.Read(s => s.ID == subsysId).FirstOrDefault();
            var subsysVmData = this.SubSysVmService.Read(s => s.ID == subsysVmId).FirstOrDefault();

            var subsysDetData =
                this.SubSysDetService.Read(
                    s => s.CompUuid == subsysData.CompUuid && s.ServiceUuid == subsysData.ServiceUuid).FirstOrDefault();
            var lastDatabaseData = this.SubSysDetService.Read(s => s.DataBaseid != null);
            var newDatabaseId = lastDatabaseData.Any()
                ? this.IntToDabaseid(this.DatabaseidToInt(lastDatabaseData.Max(s => s.DataBaseid)) + 1)
                : this.IntToDabaseid(0);
            subsysDetData.DataBaseid = newDatabaseId + WebConfigurationManager.AppSettings["ServiceType"] + "01";
            subsysDetData.ExtIP = subsysVmData.VMIP;
            subsysDetData.IsEnabled = true.ToString();

            var subsysRelData = this.SubSysRelService.Read(s => s.SubSysDetid == subsysDetData.ID).FirstOrDefault();
            subsysRelData.SubSysVMid = subsysVmId;

            PowerShellCommand powerShellCommand = new PowerShellCommand();
            string createDatabaseCmd = powerShellCommand.GetDbgenCmd(subsysDetData.DataBaseid, WebConfigurationManager.AppSettings["PowerShellIp"], subsysVmData.VMIP,
                subsysData.CompUuid.ToString(), subsysVmData.Account, subsysVmData.Password);
            TempData["command_request"] = createDatabaseCmd;
            try
            {
                TempData["command_response"] = powerShellCommand.Exec(createDatabaseCmd);
            }
            catch (Exception ex)
            {
                //報錯的話不執行下面的更新資料
                TempData["command_response"] = ex.Message;
                return RedirectToAction("Index", "Terminal");
            }

            this.SubSysDetService.Update(subsysDetData);
            this.SubSysRelService.Update(subsysRelData);

            return RedirectToAction("Index", "Terminal");
        }


        public List<DatabaseEntity> GetDatabaseList()
        {
            var subSysData = this.SubSysService.Read(null);
            var subSysDetData = this.SubSysDetService.Read(null);
            var subSysDetDataGroupByDbId = subSysDetData.Where(s => s.DataBaseid != null && s.ExtIP != null).GroupBy(s => s.DataBaseid);
            var subSysRelData = this.SubSysRelService.Read(null);
            var subSysVmData = this.SubSysVmService.Read(null);

            var result = new List<DatabaseEntity>();
            if (subSysDetDataGroupByDbId != null)
            {
                foreach (var subSysDetEntity in subSysDetDataGroupByDbId.Select(s => s.FirstOrDefault()))
                {
                    var subSysVmId = subSysRelData.FirstOrDefault(s => s.SubSysDetid == subSysDetEntity.ID).SubSysVMid;
                    var subSysVmEntity = subSysVmData.FirstOrDefault(s => s.ID == subSysVmId) ?? new SubSysVmEntity();
                    result.Add(new DatabaseEntity()
                    {
                        Id = subSysDetEntity.ID.ToString(),
                        DatabaseId = subSysDetEntity.DataBaseid,
                        DatabaseIp = subSysDetEntity.ExtIP,
                        ServiceTotal = subSysDetDataGroupByDbId.SelectMany(s => s.Where(z => z.DataBaseid == subSysDetEntity.DataBaseid)).Count().ToString(),
                        DbTotalUser = subSysData.Where(s =>
                            subSysDetData.Where(q => q.DataBaseid == subSysDetEntity.DataBaseid).Select(q => q.SubSysid).ToArray().Contains(s.ID) &&
                            s.User_max != null).Sum(s => int.Parse(s.User_max)).ToString(),
                        VmId = subSysVmEntity.ID.ToString() ?? "尚未設置",
                        VmType = subSysVmEntity.Type ?? "尚未設置",
                        VmHostName = subSysVmEntity.VMHostName ?? "尚未設置",
                        VmIp = subSysVmEntity.VMIP ?? "尚未設置",
                        VmDesc = subSysVmEntity.DESC ?? "尚未設置",
                        VmTotalUser = "1000"
                    });
                }
            }
            return result;
        }


        public int DatabaseidToInt(string databaseid)
        {
            //throw new Exception(databaseid);
            char[] charDbid = databaseid.ToArray();
            int dbname_ascii_1 = this.RetAsciiCode(charDbid[3].ToString()) - 65;
            int dbname_ascii_2 = (int)Math.Pow(26, 1) * (this.RetAsciiCode(charDbid[2].ToString()) - 65);
            int dbname_ascii_3 = (int)Math.Pow(26, 2) * (this.RetAsciiCode(charDbid[1].ToString()) - 65);
            int dbname_ascii_4 = (int)Math.Pow(26, 3) * (this.RetAsciiCode(charDbid[0].ToString()) - 65);

            //throw new Exception(dbname_ascii_2.ToString());
            return dbname_ascii_1 +
                   dbname_ascii_2 +
                   dbname_ascii_3 +
                   dbname_ascii_4;
        }


        public string IntToDabaseid(int intDbid)
        {
            //throw new Exception(intDbid.ToString());
            int quotient_1st = intDbid / 26;
            int remainder_1st = (intDbid % 26) + 65;
            int quotient_2st = quotient_1st / 26;
            int remainder_2st = (quotient_1st % 26) + 65;
            int quotient_3st = quotient_2st / 26;
            int remainder_3st = (quotient_2st % 26) + 65;
            int remainder_4st = (quotient_3st % 26) + 65;

            return this.RetAsciiChar(remainder_4st) +
                   this.RetAsciiChar(remainder_3st) +
                   this.RetAsciiChar(remainder_2st) +
                   this.RetAsciiChar(remainder_1st);
        }


        public int RetAsciiCode(string MyString)
        {
            if (MyString.Length == 0)
                return 0;
            else if (MyString.Length > 1)
                MyString = MyString[0].ToString();

            int AsciiCode = (int)System.Convert.ToChar(MyString);
            return AsciiCode;
        }


        public string RetAsciiChar(int AsciiCode)
        {
            return System.Convert.ToChar(AsciiCode).ToString();
        }



    }
}
