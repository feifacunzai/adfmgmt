﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MgmtForm.BL.Interfaces;
using MgmtForm.ViewModels;
using Newtonsoft.Json;

namespace MgmtForm.UI.Controllers
{
    public class VmController : Controller
    {

        ISubSysService SubSysService { get; set; }


        ISubSysDetService SubSysDetService { get; set; }


        ISubSysRelService SubSysRelService { get; set; }


        ISubSysVmService SubSysVmService { get; set; }


        public VmController(ISubSysService subSys, ISubSysDetService subSysDet, ISubSysRelService subSysRel, ISubSysVmService subSysVm)
        {
            this.SubSysService = subSys;
            this.SubSysDetService = subSysDet;
            this.SubSysRelService = subSysRel;
            this.SubSysVmService = subSysVm;
        }

        //
        // GET: /Company/
        public ActionResult Index()
        {
            var data = this.SubSysVmService.Read(null).OrderByDescending(s => s.CreateDate);
            return View(data);
        }


        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Post(SubSysVmEntity subSysVmEntity)
        {
            subSysVmEntity.ID = Guid.NewGuid();
            subSysVmEntity.IsEnable = true.ToString();
            this.SubSysVmService.Create(subSysVmEntity);
            return RedirectToAction("Index", "Vm");
        }



    }
}
