﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MgmtForm.BL.Interfaces;
using MgmtForm.ViewModels;
using Newtonsoft.Json;

namespace MgmtForm.UI.Controllers
{
    public class CompanyController : Controller
    {

        ISubSysService SubSysService { get; set; }


        ISubSysDetService SubSysDetService { get; set; }


        ISubSysRelService SubSysRelService { get; set; }


        ISubSysVmService SubSysVmService { get; set; }


        public CompanyController(ISubSysService subSys, ISubSysDetService subSysDet, ISubSysRelService subSysRel, ISubSysVmService subSysVm)
        {
            this.SubSysService = subSys;
            this.SubSysDetService = subSysDet;
            this.SubSysRelService = subSysRel;
            this.SubSysVmService = subSysVm;
        }

        //
        // GET: /Company/

        public ActionResult Index()
        {
            var subSysData = this.SubSysService.Read(null);
            var subSysDetData = this.SubSysDetService.Read(null);

            var result = new List<CompanyEntity>();
            if (subSysData != null)
            {
                foreach (var subSysEntity in subSysData)
                {
                    var subSysDetEntity = (subSysDetData != null) ? subSysDetData.FirstOrDefault(s => s.SubSysid == subSysEntity.ID) : null;
                    var createDate = subSysEntity.CreateDate.Split("+".ToCharArray());
                    result.Add(new CompanyEntity()
                    {
                        Id = subSysEntity.ID,
                        CompanyId = subSysEntity.CompId,
                        CompanyName = subSysEntity.Name,
                        CompanyUuid = subSysEntity.CompUuid.ToString(),
                        CreateDate = createDate[0],
                        ServiceUuid = subSysEntity.ServiceUuid.ToString(),
                        DatabaseIp = (subSysDetEntity != null && !string.IsNullOrEmpty(subSysDetEntity.ExtIP)) ? subSysDetEntity.ExtIP : null,
                        DatabaseId = (subSysDetEntity != null && !string.IsNullOrEmpty(subSysDetEntity.DataBaseid)) ? subSysDetEntity.DataBaseid : null,
                        Status = subSysEntity.Status,
                        Storage = subSysEntity.Storage_max,
                        UserLimit = subSysEntity.User_max
                    });
                }
            }
            return View(result.OrderByDescending(s => s.CreateDate));
        }


        //
        // POST: /Default1/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormCollection collection)
        {
            var modelId = Guid.Parse(collection["Id"]);
            var model = this.SubSysService.Read(s => s.ID == modelId).FirstOrDefault();
            if (model != null)
            {
                try
                {
                    model.Status = collection["Status"];
                    this.SubSysService.Update(model);
                }
                catch (Exception ex)
                {
                    TempData["AlertMessage"] = ex.Message;
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else
            {
                TempData["AlertMessage"] = "資料不存在";
            }
            return RedirectToAction("Index");
        }

    }
}
