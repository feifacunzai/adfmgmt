﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MgmtForm.BL.Interfaces;
using MgmtForm.Utility.Extensions;
using MgmtForm.ViewModels;
using Newtonsoft.Json;

namespace MgmtForm.UI.Controllers
{
    public class TerminalController : Controller
    {

        //
        // GET: /Company/
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult InvokePowerShell(FormCollection formdata)
        {
            if (string.IsNullOrEmpty(formdata["cmd"]))
            {
                throw new Exception("缺少cmd參數");
            }

            PowerShellCommand powerShellCommand = new PowerShellCommand();
            var cmdResponse = "";
            try
            {
                cmdResponse = powerShellCommand.Exec(formdata["cmd"]);
            }
            catch (Exception ex)
            {
                cmdResponse = ex.Message;
            }
            return Json(new
            {
                response = cmdResponse
            });
        }


    }
}
