﻿using System.Collections.ObjectModel;
using System.Dynamic;
using System.Reflection;
using System.Web.Script.Serialization;
using MgmtForm.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace MgmtForm.UI.ActionFilters
{
    public class ApiResultAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (filterContext.Result is JsonResult)
            {
                var result = filterContext.Result as JsonResult;
                var data = result.Data;

                dynamic entity = new ExpandoObject();
                var jsonProperty = entity as IDictionary<string, object>;
                jsonProperty.Add("ERROR_CODE", "0");
                if (data != null)
                {
                    var propertyList = data.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .ToDictionary(prop => prop.Name, prop => prop.GetValue(data, null));
                    foreach (var property in propertyList)
                    {
                        //if (property.Value == null)
                        //    continue;
                        jsonProperty.Add(property.Key, property.Value);
                    }
                }

                var serialize = new JavaScriptSerializer();
                var job = serialize.DeserializeObject(JsonConvert.SerializeObject(entity));
                result.Data = job;
            }            
        }

    }
}