﻿using System;
using System.Security.Principal;
using System.Threading;
using MgmtForm.Utility.Extensions.Authentication;
using MgmtForm.ViewModels;
using System.Web.Mvc;
using SsoReference;

namespace MgmtForm.UI.ActionFilters
{
    public class AuthorizeByTokenAttribute : AuthorizeAttribute
    {

        public IChiperTextHelper ChiperTextHelper { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //// hack for developing            
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {                
                return;
            }

            //// Get request data
            var requestData = this.GetApiRequestEntity(filterContext);

            //// Check TimeStamp
            //this.CheckIsTimeStampValid(requestData);

            ////// Check Signature
            //var userData = this.UserService.GetUserByToken(requestData.Token);
            //this.CheckIsSignatureValid(requestData, userData);

            ////// Assign User Identity
            //this.AssignUserIdentity(filterContext, userData);

            var ssoToken = this.GetValidToken(filterContext, "PRIVILEGED_APP_SSO_TOKEN");

            if (ssoToken != null)
            {
                SsoCacheCenter ssoCacheCenter = new SsoCacheCenter();
                if (ssoCacheCenter.GetValidateForeignPrivilegedToken(ssoToken).ExpireDate < DateTime.Now)
                {
                    throw new Exception("異質PrivilegedToken已失效");
                }

                //create & update uuid
                var logUuid = ssoCacheCenter.GetAppName(ssoToken);

                //// Assign User Identity
                this.AssignUserIdentity(filterContext, logUuid);

                //// Chekc user and group
                base.OnAuthorization(filterContext);

            }
        }

        private void CheckIsTimeStampValid(ApiRequestEntity requestData)
        {
            //// Check is timestamp valid
            if (!this.ChiperTextHelper.CheckTimestampInRange(requestData.TimeStamp, 86400 * 30))
            {
                throw new AuthorizeTokenFailureException("Timestamp not valid!");
            }
        }

        private void AssignUserIdentity(AuthorizationContext filterContext, string userUuid)
        {
            var identity = new GenericIdentity(userUuid, "Basic");
            var principal = new GenericPrincipal(identity, new string[] { "Basic" });
            filterContext.HttpContext.User = principal;
            Thread.CurrentPrincipal = principal;
        }

        private ApiRequestEntity GetApiRequestEntity(AuthorizationContext filterContext)
        {
            ApiRequestEntity entity = new ApiRequestEntity();

            entity.Token = this.GetDataFromValueProvider(filterContext, "token");
            entity.TimeStamp = this.GetDataFromValueProvider(filterContext, "timestamp");
            entity.Signature = this.GetDataFromValueProvider(filterContext, "signature");
            entity.Data = this.GetDataFromValueProvider(filterContext, "data");

            return entity;
        }

        private string GetValidToken(AuthorizationContext filterContext, string key)
        {
            var token = this.GetDataFromValueProvider(filterContext, key);

            return token;
        }

        private string GetDataFromValueProvider(AuthorizationContext filterContext, string key)
        {
            if (filterContext.Controller.ValueProvider.GetValue(key) != null)
            {
                return filterContext.Controller.ValueProvider.GetValue(key).AttemptedValue;
            }
            else
            {
                return null;
            }
        }
    }
}