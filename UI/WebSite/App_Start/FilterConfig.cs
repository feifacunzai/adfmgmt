﻿using MgmtForm.UI.ActionFilters;
using MgmtForm.Utility.Extensions.ExceptionHandling;
using MgmtForm.Utility.Extensions.Nlog;
using System.Web;
using System.Web.Mvc;

namespace MgmtForm.UI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ApiErrorHandleAttribute());
            filters.Add(new ExceptionHandlingAttribute());
            filters.Add(new ApiResultAttribute());
            //filters.Add(DependencyResolver.Current.GetService<LogRequestAttribute>());            
            filters.Add(DependencyResolver.Current.GetService<DeveloperHackAuthAttribute>());
        }
    }
}