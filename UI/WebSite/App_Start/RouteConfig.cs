﻿using MgmtForm.Utility.Extensions.ElmahTools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MgmtForm.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            var namespaces = new[] { "Elmah.Mvc" };
            var elmahRoute = "Errors";
            string allowedIps = ConfigurationManager.AppSettings["elmah.mvc.allowip"];

            routes.MapRoute(
                "Elmah.Override",
                string.Format("{0}/{{resource}}", elmahRoute),
                new
                {
                    controller = "Elmah",
                    action = "Index",
                    resource = UrlParameter.Optional
                },
                new { resource = new IPConstraint(allowedIps) },
                namespaces);

            routes.MapRoute(
                "Elmah.Override.Detail",
                string.Format("{0}/detail/{{resource}}", elmahRoute),
                new
                {
                    controller = "Elmah",
                    action = "Detail",
                    resource = UrlParameter.Optional
                },
                new { resource = new IPConstraint(allowedIps) },
                namespaces);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Company", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}